---
layout: home

hero:
    name:    nano/core/exception
    tagline: exception module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/core/exception is a simple and minimal exception module for nano
    -   title:   easy to use
        details: |
                 nano/core/exception is easy to use. it provides a simple and minimal interface for throwing and catching exceptions
---
