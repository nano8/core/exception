<?php

use laylatichy\nano\core\exception\NanoException;
use laylatichy\nano\core\httpcode\HttpCode;

if (!function_exists('useNanoException')) {
    function useNanoException(string $message, HttpCode $code = HttpCode::INTERNAL_SERVER_ERROR): void {
        throw new NanoException($message, $code->code());
    }
}
