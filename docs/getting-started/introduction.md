---
layout: doc
---

# introduction

## what is nano/core/exception?

`nano/core/exception` is a library that provides a set of exception classes that can be used to signal errors in your
code
