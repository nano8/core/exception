---
layout: doc
---

# installation

::: tip
if you are using nano framework, you don't need to install anything to use this module, it is already included
:::

if you are not using nano framework, you can install this module using composer

```sh
composer require laylatichy/nano-core-exception
```






