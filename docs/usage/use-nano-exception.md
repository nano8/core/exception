---
layout: doc
---

<script setup>
const args = {
    useNanoException: [
        { 
            type: 'string',
            name: 'message',
        },
        { 
            type: 'HttpCode',
            name: 'code',
        },
    ],
};
</script>

##### laylatichy\nano\core\exception\NanoException

## usage

`useNanoException()` is a helper function to throw an exception
it will be caught by the nano framework and converted to a response

```php
useNanoException() // same as throw new NanoException()
```

## <Types fn="useNanoException" r="void" :args="args.useNanoException" /> {#useNanoException}

```php
useNanoException('message', HttpCode::UNAUTHORIZED);
```

will convert to a response with the following json body and http status code 401

```json
{
    "code":     401,
    "response": "message"
}
```

you don't have to specify the http status code, it will be set to `HttpCode::INTERNAL_SERVER_ERROR` by default

```php
useNanoException('message internal server error');
```

will convert to a response with the following json body and http status code 500

```json
{
    "code":     500,
    "response": "message internal server error"
}
```
